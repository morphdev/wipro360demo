function UnityProgress(unityInstance, progress) {
  if (!unityInstance.Module)
    return;
  if (!unityInstance.logo) {
    unityInstance.logo = document.createElement("div");
    unityInstance.logo.className = "logo " + unityInstance.Module.splashScreenStyle;
    unityInstance.container.appendChild(unityInstance.logo);
  }
  if (!unityInstance.progress) {    
    unityInstance.progress = document.createElement("div");
    unityInstance.progress.className = "progress " + unityInstance.Module.splashScreenStyle;
    unityInstance.progress.empty = document.createElement("div");
    unityInstance.progress.empty.className = "empty";
    unityInstance.progress.appendChild(unityInstance.progress.empty);
    unityInstance.progress.full = document.createElement("div");
    unityInstance.progress.full.className = "full";
    unityInstance.progress.appendChild(unityInstance.progress.full);
      
    /* Sanya*/
    unityInstance.progress.loadpercent = document.createElement("div");
    unityInstance.progress.loadpercent.className = "loadpercent";
    unityInstance.progress.appendChild(unityInstance.progress.loadpercent);
    /* Sanya end*/
    unityInstance.container.appendChild(unityInstance.progress);
  }
    
  /* Sanya*/
  unityInstance.progress.loadpercent.innerHTML = ((Math.ceil(100 * progress)) + "%");
  unityInstance.progress.loadpercent.style.right = ((100 * (1 - progress)) + "%");
  /* Sanya end*/

    unityInstance.progress.full.style.width = (100 * progress) + "%";
  unityInstance.progress.empty.style.width = (100 * (1 - progress)) + "%";
  if (progress == 1)
    unityInstance.logo.style.display = unityInstance.progress.style.display = "none";
}
